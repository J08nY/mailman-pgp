from distutils.version import LooseVersion

__author__ = 'Jan Jancar'
__copyright__ = 'Copyright (C) 2017 Jan Jancar'
__license__ = 'GPLv3'
__version__ = str(LooseVersion('0.3.0'))
