========================
List encryption settings
========================

Mailman-pgp stores per-list encryption configuration. The defaults are listed
in the parentheses after the attribute name.

 - ``nonencrypted_msg_action(Action.reject)``, an action to take when a message
   is received not encrypted.
 - ``encrypt_outgoing(True)``, whether to encrypt the outgoing postings to the
   subscribers keys.
