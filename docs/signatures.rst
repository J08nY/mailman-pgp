=======================
List signature settings
=======================

Mailman-pgp stores a per-list signature handling configuration. Several actions
and attributes are configurable. They are processed in order they are defined 
here, if one matches and the action set is not ``Action.defer`` it is taken on
the message. Defaults are listed in the parentheses after the attribute name.

 - ``unsigned_msg_action(Action.reject)``, is taken if a message is not recognized
   as PGP signed (PGP/MIME and inline PGP is tried).
 - ``inline_pgp_action(Action.defer)``, is taken if the message is Inline PGP
   signed.
 - ``expired_sig_action(Action.reject)``, is taken if the message signature or
   key its made by is expired at the moment of verification.
 - ``revoked_sig_action(Action.reject)``, is taken if the key that signed the
   message was revoked.
 - ``invalid_sig_action(Action.reject)``, is taken if the message signature doesn't
   verify or is otherwise invalid.
 - ``duplicate_sig_action(Action.reject)``, is taken if the message contains a signature
   by the senders key that was previously sent to the mailing list. Signature hashes
   are collected only if the ``[misc]collect_sig_hashes`` config option is set to yes.
 
Other options set how Mailman-pgp handles the signature:

 - ``strip_original_sig(False)``, whether to strip the original users signature
   (if any), from the posting.
 - ``sign_outgoing(False)``, whether outgoing postings should be signed by the
   list key.
